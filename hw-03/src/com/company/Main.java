package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;

public class Main {

    public static void main(String[] args) {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            while (true) {
                System.out.println("Enter something : ");
                String input = br.readLine();

                String[] split = input.split(" ");
//                System.out.println(Arrays.toString(split));


                if ("q".equals(input)) {
                    System.out.println("Quit!");
                    System.exit(0);
                }

                //validation block


                System.out.println("input : " + input);
                System.out.println("-----------\n");
                calc(split);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void calc(String[] args) {
        if (args.length < 3) {
            System.err.println("Usage: Java Calc operand operator operand");

            return;
        }

        BigDecimal leftOperand = new BigDecimal(args[0]);
        BigDecimal rightOperand = new BigDecimal(args[2]);
        String operand = args[1];
        BigDecimal result;
        switch (operand) {
            case "+":
                result = leftOperand.add(rightOperand);
                break;
            case "-":
                result = leftOperand.subtract(rightOperand);
                break;
            case "*":
                result = leftOperand.multiply(rightOperand);
                break;
            case "/":
                result = leftOperand.divide(rightOperand, BigDecimal.ROUND_HALF_EVEN);
                break;
            default:
                System.out.println("Invalid operator");
                return;
        }
        System.out.printf("leftOperand=%s; rightOperand=%s; operand=%s; result=%s", leftOperand, rightOperand, operand, result).println();
    }

}
