package com.company;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        int min = -50;
        int max = 100;
        int max_value = 0, min_value = 0;
        int[] multipleRaw = {1, 1, 1, 1, 1, 1, 1, 1};
        long[] multipleColumn = {1, 1, 1, 1, 1};

        int[][] array = new int[8][5];
        Random random = new Random();

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = random.nextInt(max - min + 1) + min;

            }

        }

        int maxMultipleRaw = 0;
        int maxMultipleRawId = 0;
        long maxMultipleColumn = 0;
        int maxMultipleColumnId = 0;
        System.out.println("Random array [8][5] was generated:");
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {

                System.out.print(String.format("|%4d| ", array[i][j]));
                if (max_value <= array[i][j])
                    max_value = array[i][j];
                if (min_value >= array[i][j])
                    min_value = array[i][j];
                multipleRaw[i] = multipleRaw[i] * Math.abs(array[i][j]);
                multipleColumn[j] = multipleColumn[j] * Math.abs(array[i][j]);

                if (i == 0) {
                    if (maxMultipleColumn < multipleColumn[j]) {
                        maxMultipleColumn = multipleColumn[j];
                        maxMultipleColumnId = j;

                    }
                }
            }

            if (maxMultipleRaw < multipleRaw[i]) {
                maxMultipleRaw = multipleRaw[i];
                maxMultipleRawId = i;
            }
            System.out.println();
        }
        System.out.println("Максимальное значение элемента  массива " + max_value);
        System.out.println("Минимальное значение элемента  массива " + min_value);
        System.out.println("Индекс строки с наибольшим по модулю произведением 2-х элементов " + maxMultipleRawId);
        System.out.println("Индекс строки с наименьшим по модулю произведением 2-х элементов " + maxMultipleColumnId);
        for (int i = 0; i < array.length; i++) {


        }
    }
}

